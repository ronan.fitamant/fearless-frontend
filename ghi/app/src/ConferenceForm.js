import React, { useEffect, useState} from "react";

function ConferenceForm () {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = presentations;
        data.max_attendees = attendee;
        data.location = location;

        console.log(data);

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setPresentations('');
            setAttendee('');
            setLocation('');
        }
    }

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [start, setStart] = useState('');
    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const [end, setEnd] = useState('');
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [presentations, setPresentations] = useState('');
    const handlePresentationChange = (event) => {
        const value = event.target.value;
        setPresentations(value);
    }

    const [attendee, setAttendee] = useState('');
    const handleAttendeeChange = (event) => {
        const value = event.target.value;
        setAttendee(value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations)
        }
      }

      useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStartChange} value={start} placeholder="Starts" required type="date" className="form-control" id="start-date" name="starts"/>
                    <label htmlFor="starts">Start Date</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleEndChange} value={end} placeholder="Ends" required type="date" className="form-control" id="end-date" name="ends"/>
                    <label htmlFor="starts">End Date</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="description">Description</label>
                    <textarea onChange={handleDescriptionChange} value={description} placeholder="Description" required type="text" className="form-control" id="description" name="description"></textarea>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePresentationChange} value={presentations} placeholder="Max Presentations" required type="number" className="form-control" id="max-presentations" name="max_presentations"/>
                    <label htmlFor="max-presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleAttendeeChange} value={attendee} placeholder="Max Attendees" required type="number" className="form-control" id="max-attendees" name="max_attendees"/>
                    <label htmlFor="max-attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                            return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
