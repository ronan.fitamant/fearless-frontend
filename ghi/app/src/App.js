import Nav from './Nav';
import AttendeeList from './AttendeeList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container">
      <ConferenceForm />
      {/* <LocationForm />
      <AttendeeList attendees={props.attendees} /> */}
    </div>
    </>
  );
}

export default App;
